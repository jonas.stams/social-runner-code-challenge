# Social Runner Coding Challenge

Auteur: Jonas Stams\
Datum: 12/10/2021

[Bekijk opdracht][https://gist.github.com/jelleverheyen/3eb615e36e5d22476ce66801c6ef882a]

## Opmerkingen
- Dit gedeelte heb ik niet gedaan omdat ik niet precies begrijp wat je verwacht -> `Pagination van 10 of alle 11 laten zien maar gewoon in opgesplitste calls (soort van virtual scroll)?.` 
    - Laat data van alle gezochte symbolen in een lijst zien zoals in het design. Note: Er kunnen max 10 symbolen opgezocht worden, als er meer dan 10 ingegeven worden, batch dan de api calls. (e.g. er worden er 11 gezocht, doe een query met 10 symbolen en een query met het elfde symbool)
- Heb enkel `components/StockSearchBar/StockSearchBar.tsx` getest om te laten zien dat ik het kan.
- API is niet gratis dus ik return mock data in `api/yahoo-finance/api.ts`, je kan wel testen met echte data door die mock promises in comment te zetten
- Coding challenge is vrij uitgebreid voor code skills te bekijken (No hard feelings haha 😄 just some brain food)
    - Denk hierover na voor de volgende deelnemers. Als freelancer ben ik hier een aantal uur aan verloren die ik niet heb kunnen werken = geld misgelopen
    - Er zijn misschien delen die nog beter gerefactored of in aparte components gestoken kunnen worden, maar wou er ook niet te veel tijd in steken
    



## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

[https://gist.github.com/jelleverheyen/3eb615e36e5d22476ce66801c6ef882a]: https://gist.github.com/jelleverheyen/3eb615e36e5d22476ce66801c6ef882a
