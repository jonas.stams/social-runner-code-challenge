import {optionsMockData, quoteMockData, YahooFinanceOptionResponse, YahooFinanceQuoteResponse} from "./types";
import axios from "axios";

const baseUrl = 'https://yfapi.net';
const apiKey = '1ucymaBOCI23XLyaZT0tA6PT2k4D9d1t1VB9RFuS';
const options = {headers: {'x-api-key': apiKey}};

export default {
    getQuote(symbols: string): Promise<YahooFinanceQuoteResponse> {

        return new Promise(resolve => {
            setTimeout(() => resolve(quoteMockData), 3000)
        });
        return axios.get(`${baseUrl}/v6/finance/quote?symbols=${symbols}`, options)
            .then((response) => {
                console.log('[YAHOO_FINANCE_API]: DATA RECEIVED', response);
                return response.data;
            }).catch(err => {
                console.log('[YAHOO_FINANCE_API]: ERROR', err);
                throw err;
            })
    },
    getOptions(symbol: string, date?: number): Promise<YahooFinanceOptionResponse> {
        return new Promise(resolve => {
            setTimeout(() => resolve(optionsMockData), 3000)
        });

        return axios.get(`${baseUrl}/v7/finance/options/${symbol}${date ? '?date=' + date : ''}`, options)
            .then((response) => {
                console.log('[YAHOO_FINANCE_API]: DATA RECEIVED', response);
                return response.data;
            }).catch(err => {
                console.log('[YAHOO_FINANCE_API]: ERROR', err);
                throw err;
            })
    }
}
//TODO: Move these interfaces to corect place
