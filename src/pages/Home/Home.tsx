import React, {useState} from 'react'
import StockSearchBar from '../../components/StockSearchBar/StockSearchBar'
import {Alert, Container, Grid} from '@mui/material'
import './Home.css'
import api from '../../api'
import {YahooFinanceQuote} from '../../api/yahoo-finance/types'
import QuoteListItem from '../../components/QuoteListItem/QuoteListItem'
import {useStockContext} from '../../context/stock/useStockContext'
import {useHistory} from 'react-router-dom'

const Home = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [error, setError] = useState<string>('')
  const history = useHistory()
  const { setSearch, quotes, setQuotes, setSelectedQuote } = useStockContext()

  const searchSymbols = (symbols: string) => {
    setSearch(symbols)
    setError('')
    setIsLoading(true)

    api.YahooFinance.getQuote(symbols)
      .then(({ quoteResponse }) => {
        if (quoteResponse.error) {
          // Weet niet zeker of quoteResponse.error een string is, maar hier ga ik nu even van uit aangezien dit niet is gedocumenteerd in API SPEC
          return setError(quoteResponse.error)
        }

        setQuotes(quoteResponse.result)
      })
      .catch(() => {
        setError('Error occurred while getting data')
      })
      .finally(() => setIsLoading(false))
  }

  const goTopOptions = (quote: YahooFinanceQuote) => {
    setSelectedQuote(quote)
    history.push(`/stock/${quote.symbol}`)
  }

  return (
    <Container className="home">
      <Grid container={true} direction="row" justifyContent="center" alignItems="center">
        <Grid className="search-bar-container" item={true} md={8} xs={12}>
          <StockSearchBar onSearch={searchSymbols} isLoading={isLoading} />
        </Grid>
        <Grid item={true} md={8} xs={12}>
          {error && <Alert severity="warning">{error}</Alert>}

          {quotes.map((quote) => (
            <QuoteListItem key={quote.symbol} quote={quote} goToOptions={goTopOptions} />
          ))}
        </Grid>
      </Grid>
    </Container>
  )
}

export default Home
