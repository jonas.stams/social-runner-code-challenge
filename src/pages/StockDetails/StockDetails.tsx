import {useParams} from 'react-router-dom'
import React, {useEffect, useState} from 'react'
import {YahooFinanceOptionResult} from '../../api/yahoo-finance/types'
import api from '../../api/yahoo-finance/api'
import moment from 'moment'
import {Alert, Container, Grid, MenuItem, Select, SelectChangeEvent} from '@mui/material'
import './StockDetails.css'
import StockOptionsTable from '../../components/StocsOptionsTable/StockOptionsTable'
import {useStockContext} from '../../context/stock/useStockContext'

export default function StockDetails() {
    const {symbol} = useParams<{ symbol: string }>()
    const [isLoading, setIsLoading] = useState<boolean>(false)
    const [error, setError] = useState<string>('')
    const [expirationDate, setExpirationDate] = useState<number>()
    const [data, setData] = useState<YahooFinanceOptionResult>()
    const {selectedQuote, setSelectedQuote} = useStockContext()

    const getStockData = (date?: number) => {
        setIsLoading(true)

        api
            .getOptions(symbol, date)
            .then(({optionChain}) => {
                if (optionChain.error) {
                    setError(optionChain.error)
                    return
                }

                if (!optionChain.result.length) {
                    setError('No data found for this symbol')
                    return
                }

                const {options, quote} = optionChain.result[0]
                setSelectedQuote(quote)
                if (!options.length || (!options[0].calls.length && !options[0].puts.length)) {
                    setError('No data found for this symbol')
                    return
                }

                setData(optionChain.result[0])
            })
            .catch(() => {
                setError('Error occurred while getting data')
            })
            .finally(() => setIsLoading(false))
    }

    const changeExpirationDate = (event: SelectChangeEvent<number>) => {
        const date = +event.target.value
        setExpirationDate(date)
        getStockData(date)
    }

    useEffect(() => {
        if (data && data.expirationDates && data.expirationDates.length) {
            setExpirationDate(data.expirationDates[0])
        }

        if (data && data.quote) {
            setSelectedQuote(data.quote)
        }

        if (!data) {
            getStockData()
        }
        // eslint-disable-next-line
    }, [data])

    return (
        <Container className="stock-details">
            <Grid container={true} justifyContent="center">
                <Grid item={true} md={8} xs={12} className="details-header">
                    {selectedQuote && (
                        <h2>
                            {selectedQuote?.shortName} ({selectedQuote?.symbol})
                        </h2>
                    )}
                    <div>
                        {expirationDate ? (
                            <Select
                                className="expiration-date-select"
                                value={expirationDate}
                                onChange={changeExpirationDate}
                            >
                                {data?.expirationDates.map((date) => (
                                    <MenuItem key={date} value={date}>
                                        {moment.unix(date).format('DD MMMM YYYY')}
                                    </MenuItem>
                                ))}
                            </Select>
                        ) : (
                            ''
                        )}
                    </div>
                </Grid>
                <Grid item={true} md={8} xs={12} className="table-wrapper">
                    {!data && !error && <h1>Loading...</h1>}
                    {error && <Alert severity="warning">{error}</Alert>}
                    {data && data.options?.length && data.options[0].calls ? (
                        <StockOptionsTable
                            rows={[...data.options[0].calls, ...data.options[0].puts]}
                            isLoading={isLoading}
                        />
                    ) : (
                        ''
                    )}
                </Grid>
            </Grid>
        </Container>
    )
}
