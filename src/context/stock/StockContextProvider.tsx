import {FC, useState} from 'react'
import {YahooFinanceQuote} from '../../api/yahoo-finance/types'
import StockContext from './context'

const StockContextProvider: FC = ({ children }) => {
  const [search, setSearch] = useState<string>('')
  const [quotes, setQuotes] = useState<YahooFinanceQuote[]>([])
  const [selectedQuote, setSelectedQuote] = useState<YahooFinanceQuote | null>(null)

  return (
    <StockContext.Provider
      value={{ search, setSearch, quotes, setQuotes, selectedQuote, setSelectedQuote }}
    >
      {children}
    </StockContext.Provider>
  )
}

export default StockContextProvider
