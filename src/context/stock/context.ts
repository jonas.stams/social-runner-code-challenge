import {YahooFinanceQuote} from "../../api/yahoo-finance/types";
import {createContext} from "react";

export type StockContextType = {
    search: string;
    setSearch: (str: string) => void,
    quotes: YahooFinanceQuote[];
    setQuotes: (quotes: YahooFinanceQuote[]) => void;
    selectedQuote: YahooFinanceQuote | null;
    setSelectedQuote: (quote: YahooFinanceQuote) => void;
}

const StockContext = createContext<StockContextType>({
    search: "",
    setSearch: () => {},
    quotes: [],
    setQuotes: () => {},
    selectedQuote: null,
    setSelectedQuote: () => {}
});

export default StockContext;
