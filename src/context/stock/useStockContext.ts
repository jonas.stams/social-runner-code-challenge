import {useContext} from "react";
import StockContext from "./context";

export const useStockContext = () => {
    return useContext(StockContext);
};
