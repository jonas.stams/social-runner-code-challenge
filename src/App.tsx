import React, {FC} from 'react'
import './App.css'
import {Route, Switch} from 'react-router-dom'
import Home from './pages/Home/Home'
import StockDetails from './pages/StockDetails/StockDetails'
import NavBar from './components/NavBar/NavBar'

const App: FC = () => {
  return (
    <div className="App">
      <NavBar />
      <Switch>
        <Route exact={true} path="/" component={Home} />
        <Route exact={true} path="/stock/:symbol" component={StockDetails} />
      </Switch>
    </div>
  )
}

export default App
