import React, {FC} from 'react'
import {Grid} from '@mui/material'

type QuoteDataProps = {
  label: string
  value: string | number
  change?: number
  changePercentage?: number
}
const QuoteData: FC<QuoteDataProps> = ({ label, value, change, changePercentage }) => {
  return (
    <Grid item={true} xs="auto">
      <p className="symbol-label">{label}</p>
      <p className="symbol-value">{value}</p>
      {change && (
        <p className={`m-0 market-change ${change >= 0 ? 'positive' : 'negative'}`}>
          {change?.toFixed(2)} ({changePercentage?.toFixed(2)}%)
        </p>
      )}
    </Grid>
  )
}

export default QuoteData
