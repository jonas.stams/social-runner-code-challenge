import React, {FC} from 'react'
import {YahooFinanceQuote} from '../../api/yahoo-finance/types'
import {Card, Grid, Link} from '@mui/material'
import './QuoteListItem.css'
import QuoteData from './components/QuoteData'

type QuoteListItemProps = {
  quote: YahooFinanceQuote
  goToOptions: (quote: YahooFinanceQuote) => void
}
const QuoteListItem: FC<QuoteListItemProps> = ({ quote, goToOptions }) => {
  const navigateToOptions = () => goToOptions(quote)
  return (
    <Card
      sx={{
        flexDirection: 'column',
        justifyContent: 'flex-start',
        padding: '10px 10px 30px 50px',
        marginBottom: '5px',
      }}
      elevation={0}
    >
      <div className="link-container">
        <Link onClick={navigateToOptions}>View options</Link>
      </div>
      <h2>
        {quote.shortName} ({quote.symbol})
      </h2>
      <Grid container={true} justifyContent="flex-start" sx={{ textAlign: 'left' }} spacing={5}>
        <QuoteData
          label="Today"
          value={quote.regularMarketPrice}
          change={quote.regularMarketChange}
          changePercentage={quote.regularMarketChangePercent}
        />
        <QuoteData label="Previous close" value={quote.regularMarketPreviousClose} />
        <QuoteData label="Open" value={quote.regularMarketOpen} />
        <QuoteData label="52 Week range" value={quote.fiftyTwoWeekRange} />
      </Grid>
    </Card>
  )
}

export default QuoteListItem
