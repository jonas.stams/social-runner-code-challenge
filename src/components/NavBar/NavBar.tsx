import React from 'react'
import {AppBar, IconButton, Toolbar, Typography} from '@mui/material'
import {ChevronLeft} from '@mui/icons-material'
import './NavBar.css'
import {Link, useLocation} from 'react-router-dom'

const NavBar = () => {
  const location = useLocation()

  return (
    <AppBar className="App-bar" elevation={0} position="static">
      <Toolbar variant="dense">
        {location.pathname !== '/' && (
          <Link to="/">
            <IconButton edge="start" color="inherit" aria-label="menu" sx={{ mr: 2 }}>
              <ChevronLeft />
            </IconButton>
          </Link>
        )}

        <Typography variant="h6" color="inherit" component="div">
          Stocks
        </Typography>
      </Toolbar>
    </AppBar>
  )
}

export default NavBar
