import {fireEvent, render, screen} from '@testing-library/react'
import React from 'react'
import StockSearchBar from './StockSearchBar'
import StockContextProvider from '../../context/stock/StockContextProvider'

const setup = (isLoading = false) => {
  const onSearchSpy = jest.fn()
  const searchBar = render(
    <StockContextProvider>
      <StockSearchBar onSearch={onSearchSpy} isLoading={isLoading} />
    </StockContextProvider>
  )
  const searchButton = screen.getByTestId('stock-search-button')
  const input = screen.getByTestId('stock-search-input') as HTMLInputElement
  return {
    searchBar,
    searchButton,
    input,
    onSearchSpy,
  }
}

describe('StockSearchBar component', () => {
  test('renders search bar', () => {
    const { searchButton, input } = setup()
    expect(searchButton).toBeInTheDocument()
    expect(input).toBeInTheDocument()
  })

  test('search button should be disabled if input is empty', async () => {
    const { searchButton } = setup()
    expect(searchButton).toBeDisabled()
  })

  test('search button should be enabled when input is not empty', async () => {
    const { searchButton, input } = setup()
    fireEvent.change(input, { target: { value: 'AAPL' } })
    expect(input.value).toBe('AAPL')
    expect(searchButton).toBeEnabled()
  })

  test('should show SearchIcon when not loading', async () => {
    setup()
    const searchIcon = screen.queryByTestId('stock-search-icon')
    const loader = screen.queryByTestId('stock-search-loader')
    expect(searchIcon).toBeInTheDocument()
    expect(loader).not.toBeInTheDocument()
  })

  test('should show Loader when is loading', async () => {
    setup(true)
    const searchIcon = screen.queryByTestId('stock-search-icon')
    const loader = screen.queryByTestId('stock-search-loader')
    expect(searchIcon).not.toBeInTheDocument()
    expect(loader).toBeInTheDocument()
  })

  test('should call not onSearch when search button is disabled and clicked', async () => {
    const { searchButton, input, onSearchSpy } = setup()

    fireEvent.click(searchButton)
    expect(onSearchSpy).not.toHaveBeenCalled()
  })
  test('should call onSearch when search button clicked', async () => {
    const { searchButton, input, onSearchSpy } = setup()

    fireEvent.change(input, { target: { value: 'AAPL' } })
    fireEvent.click(searchButton)
    expect(onSearchSpy).toHaveBeenCalled()
  })
})
