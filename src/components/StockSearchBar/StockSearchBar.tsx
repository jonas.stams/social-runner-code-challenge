import React, {ChangeEvent, FC, FormEvent} from 'react'
import './StockSearchBar.css'
import {Button, Card, CircularProgress, Grid} from '@mui/material'
import SearchIcon from '@mui/icons-material/Search'
import {useStockContext} from '../../context/stock/useStockContext'

type StockSearchBarProps = { onSearch: (e: string) => void; isLoading: boolean }

const StockSearchBar: FC<StockSearchBarProps> = ({ onSearch, isLoading }) => {
  const { search, setSearch } = useStockContext()
  const searchChange = (e: ChangeEvent<HTMLInputElement>) => setSearch(e.target.value)

  const onSubmitForm = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    if (search.length > 0) {
      onSearch(search)
    }
  }

  return (
    <form onSubmit={onSubmitForm}>
      <Card elevation={0} className="searchbar-card">
        <Grid container={true} justifyContent="space-between" spacing={2}>
          <Grid item={true} xs="auto">
            {isLoading ? (
              <CircularProgress disableShrink={true} size={35} data-testid="stock-search-loader" />
            ) : (
              <SearchIcon color="primary" fontSize="large" data-testid="stock-search-icon" />
            )}
          </Grid>
          <Grid item={true} xs={true}>
            <input
              type="text"
              placeholder="Enter multiple symbols - separated by comma"
              value={search}
              data-testid="stock-search-input"
              onChange={searchChange}
            />
          </Grid>
          <Grid item={true} md={3} xs={4} className="button-grid">
            <Button
              type="submit"
              variant="contained"
              disabled={isLoading || search.length === 0}
              data-testid="stock-search-button"
            >
              Search
            </Button>
          </Grid>
        </Grid>
      </Card>
    </form>
  )
}

export default StockSearchBar
