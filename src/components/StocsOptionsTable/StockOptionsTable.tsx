import React, {FC} from 'react'
import {DataGrid, GridRowData} from '@mui/x-data-grid'
import {YahooFinanceOption} from '../../api/yahoo-finance/types'
import moment from 'moment'
import {LinearProgress} from '@mui/material'

type StockOptionsTableProps = {
  rows: YahooFinanceOption[]
  isLoading: boolean
}
const StockOptionsTable: FC<StockOptionsTableProps> = ({ rows, isLoading }) => {
  const columns = [
    { field: 'contractSymbol', headerName: 'Contract name', flex: 1, minWidth: 200 },
    {
      field: 'lastTradeDate',
      headerName: 'Last trade date',
      flex: 1,
      minWidth: 175,
      valueGetter: (value: any) => moment.unix(value.value).format('YYYY-MM-DD hh:mmA '),
    },
    { field: 'strike', headerName: 'Strike', flex: 1, minWidth: 50 },
    {
      field: 'lastPrice',
      headerName: 'Last price',
      flex: 1,
      minWidth: 90,
      // valueGetter: (value: any) => value.toFixed(2)
    },
  ]

  const rowId = (row: GridRowData) => row.contractSymbol

  return (
    <>
      {isLoading && <LinearProgress />}
      <div style={{ height: '70vh', width: '100%', borderColor: 'white', borderWidth: 1 }}>
        <DataGrid
          getRowId={rowId}
          rows={rows}
          columns={columns}
          pageSize={50}
          rowsPerPageOptions={[5, 10, 25, 50, 100]}
        />
      </div>
    </>
  )
}

export default StockOptionsTable
